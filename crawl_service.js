var Promise = require("promise");
var jsdom = require("jsdom");

module.exports = {
  crawlSongs: crawlSongs
};

var parseDom = Promise.denodeify(jsdom.env);

function crawlSongs() {
    return parseDom('http://www.radioultra.ru/onair', ["http://code.jquery.com/jquery.js"])
        .then(function(window) {
            var $ = window.$;
            var songs = [];
            $("div.onairContent ul ul li").each(function() {
                var song = $(this).clone().children().remove().end().text();
                console.log("Fetched song: %s", song);
                var performerAndTitle = song.split(" - ");
                song = {
                    performer: performerAndTitle[0],
                    title: performerAndTitle[1]
                };
                songs.push(song);
            });
            return songs;
        });
}