var express = require("express");
var SongService = require("./song_service");
var CrawlService = require("./crawl_service");
var YoutubeService = require("./youtube_service");
var utils = require("./utils");

var CRAWLING_INTERVAL = 30; // in minutes
var CRAWLING_ON = true;

var app = express();
app.set('port', (process.env.PORT || 5000));
app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get("/", function(req, res) {
	res.render('index');
});

app.get("/db_songs", function(req, res) {
	SongService.fetchSongs().nodeify(function(err, songs) {
		if (err) {
			res.json(err);
			return;
		}
		res.json({
			total_nr_songs: songs.length,
			songs: songs
		});
	});
});

app.get("/youtube_songs", function(req, res) {
	YoutubeService.fetchSongsFromPlaylist().nodeify(function(err, songs) {
		if (err) {
			res.json(err);
			return;
		}

		res.json({
			total_nr_songs: songs.length,
			songs: songs
		});
	});
});

app.get("/crawl_start", function(req, res) {
	if (CRAWLING_ON) {
		res.json({status: "ALREADY STARTED"});
	} else {
		CRAWLING_ON = true;

		res.json({status: "STARTED"});
	}
});

app.get("/crawl_stop", function(req, res) {
	if (!CRAWLING_ON) {
		res.json({status: "ALREADY STOPPED"});
	} else {
		CRAWLING_ON = false;

		res.json({status: "STOPPED"});
	}
});

app.get("/populate", function(req, res) {
	populateYoutubePlaylist(function(err, result) {
		if (err) {
			res.json(err);
			return;
		}
		res.json(result);
	});
});

app.get("/auth_youtube", function(req, res) {
	res.redirect(YoutubeService.getAuthURL());
});

app.get("/oauth2callback", function(req, res) {
	YoutubeService.obtainAccessToken(req.query.code).nodeify(function(err, result) {
		if (err) {
			console.error("Could not obtain access token: %j", err);
			res.json(err);
			return;
		}
		res.redirect("/");
	});
});

SongService.initDB().nodeify(function(err, result) {
	if (err) {
		console.error(err);
	} else {
		console.log("Successfully initialised DB");
		app.listen(app.get('port'), function() {
			console.log('Node app is running on port', app.get('port'));
			startCrawling();
		});
	}
});

function startCrawling() {
	if (CRAWLING_ON) {
		CrawlService.crawlSongs().then(function(songs) {
			SongService.insertSongs(songs);
		}, function(err) {
			console.error(err);
		});
	}
	setTimeout(startCrawling, CRAWLING_INTERVAL * 60 * 1000);
}

function populateYoutubePlaylist() {
	return YoutubeService.fetchSongsFromPlaylist().then(function(youtubeSongsTitles) {
		return SongService.fetchSongs().then(function(dbSongs) {
			dbSongs.forEach(function(dbSong) {
				YoutubeService.searchSong(dbSong).then(function(youtubeSong) {
					if (!utils.titleExistsInList(youtubeSong.title, youtubeSongsTitles)) {
						console.log("Song %j not found in YouTube playlist", dbSong);
						YoutubeService.insertSongIntoPlaylist(youtubeSong.id, function(youtubeError, result) {});
					}
				});
			});
			return {status: "OK"};
		});
	});
}