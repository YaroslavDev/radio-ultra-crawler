var pg = require("pg");
var Promise = require("promise");
var util = require("./utils");

var DB_URL = (process.env.DATABASE_URL || "postgres://iaroslav:toor@localhost/postgres");

module.exports = {
    initDB: initDB,
    insertSong: insertSong,
    insertSongs: insertSongs,
    fetchSongs: fetchSongs
};

function initDB() {
    return dbConnect().then(function(conn) {
        var client = conn.client;
        var done = conn.done;
        var sql = "CREATE TABLE IF NOT EXISTS songs (performer VARCHAR(200), title VARCHAR(200), PRIMARY KEY (performer, title))";
        return executeQuery(client, sql, done);
    });
};

function insertSong(song) {
    return dbConnect().then(function(conn) {
        var client = conn.client;
        var done = conn.done;
        song.performer = song.performer.replace(/'/g, "");
        song.title = song.title.replace(/'/g, "");
        var sql = "INSERT INTO songs (performer, title) values ('{0}', '{1}')".format(song.performer, song.title);
        console.log("Executing SQL: %s", sql);
        return executeQuery(client, sql, done).then(function(result) {
            console.log("Successfully inserted song: %j", song);
            return song;
        }, function(err) {
            if (err.detail && err.detail.indexOf("already exists") > -1) {
                console.log("Skipping song %j. Already exists.", song);
            } else {
                throw err;
            }
            return song;
        });
    });
};

function insertSongs(songs) {
    songs.forEach(function(song) {
        insertSong(song).catch(function(err) {
            console.error("Could not insert song %j\nCause: %j", song, err);
        });
    });
}

function fetchSongs() {
    return dbConnect().then(function(conn) {
        var client = conn.client;
        var done = conn.done;
        var sql = "SELECT * FROM songs";
        return executeQuery(client, sql, done);
    });
};

function dbConnect() {
    return new Promise(function(resolve, reject) {
        pg.connect(DB_URL, function(err, client, done) {
            if (err) {
                reject(err);
            } else {
                resolve({client: client, done: done});
            }
        });
    });
};

function executeQuery(client, sql, done) {
    return new Promise(function(resolve, reject) {
        client.query(sql, function(err, result) {
            done();
            if (err) reject(err);
            else resolve(result.rows);
        });
    });
}