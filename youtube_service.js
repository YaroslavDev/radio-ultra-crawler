var fs = require('fs');
var google = require('googleapis');
var googleAuth = require('google-auth-library');
var Promise = require('promise');
var utils = require('./utils');

var SCOPES = ['https://www.googleapis.com/auth/youtube'];
var TOKEN_DIR = './credentials/';
var TOKEN_PATH = TOKEN_DIR + 'youtube_token.json';
var PLAYLIST_ID = (process.env.PLAYLIST_ID || "PLpAbQPlIczcMQI6QZwxW2kgEa0lu3SOcz");
var CLIENT_ID = process.env.CLIENT_ID;
var CLIENT_SECRET = process.env.CLIENT_SECRET;
var REDIRECT_URL = process.env.REDIRECT_URL;

module.exports = {
    insertSongIntoPlaylist: submitJobForSongInsertion,
    fetchSongsFromPlaylist: fetchSongsFromPlaylist,
    searchSong: searchSong,
    getAuthURL: getAuthURL,
    obtainAccessToken: obtainAccessToken
};

var insert_song_jobs = [];
var youtube = google.youtube('v3');
var youtubePlaylistItemsList = Promise.denodeify(youtube.playlistItems.list);
var youtubeSearchList = Promise.denodeify(youtube.search.list);
var youtubePlaylistItemsInsert = Promise.denodeify(youtube.playlistItems.insert);

function searchSong(song) {
    return authorize().then(function(auth) {
        console.log("Searching YouTube for %j", song);
        return youtubeSearchList({
            auth: auth,
            part: "id,snippet",
            q: song.performer + ' ' + song.title,
            maxResults: 1
        }).then(function(response) {
            if (response.items == null || response.items[0] == null) {
                throw "Song {0} not found on YouTube".format(song);
            }
            return {
                id: response.items[0].id,
                title: response.items[0].snippet.title
            };
        });
    });
}

function fetchSongsFromPlaylist() {
    return authorize().then(function(auth) {
        var youtube = google.youtube('v3');
        var songs = [];
        return fetchSongsFromPage(youtube, auth, null, songs);
    });
}

function fetchSongsFromPage(youtube, auth, pageToken, allSongs) {
    var queryObj = {
        auth: auth,
        part: "snippet",
        playlistId: PLAYLIST_ID,
        maxResults: 50
    };
    if (pageToken) {
        queryObj.pageToken = pageToken;
    }
    return youtubePlaylistItemsList(queryObj).then(function(response) {
        response.items.forEach(function (item) {
            allSongs.push(item.snippet.title);
        });
        var nextPageToken = response.nextPageToken;
        if (!nextPageToken) {
            return allSongs;
        }
        return fetchSongsFromPage(youtube, auth, nextPageToken, allSongs);
    });
}

function submitJobForSongInsertion(song) {
    return new Promise(function(resolve, reject) {
        insert_song_jobs.push({
            song: song,
            resolve: resolve,
            reject: reject
        });
    });
}

function insertSongIntoPlaylistDelayed() {
    if (insert_song_jobs.length > 0) {
        var next_job = insert_song_jobs.shift();
        insertSongIntoPlaylist(next_job.song, next_job.resolve, next_job.reject).nodeify(function(err, result) {
            if (err) {
                next_job.reject(err);
                return;
            }
            next_job.resolve(result);
        });
    }
    setTimeout(function() {
       insertSongIntoPlaylistDelayed();
    }, 2000);
}

insertSongIntoPlaylistDelayed();

function insertSongIntoPlaylist(youtubeSongId) {
    return authorize().then(function(auth) {
        console.log("Inserting song with id %j into playlist", youtubeSongId);
        return youtubePlaylistItemsInsert({
            auth: auth,
            part: "id,snippet",
            resource: {
                snippet: {
                    playlistId: PLAYLIST_ID,
                    resourceId: youtubeSongId
                }
            }
        }).then(function(response) {
            console.log("Inserted song with id %j \nYoutube result: %j", youtubeSongId, response);
            return response;
        });
    });
}

function getAuthClient() {
    var auth = new googleAuth();
    return new auth.OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);
}

function authorize() {
    var oauth2Client = getAuthClient();
    // Check if we have previously stored a token.
    var readFile = Promise.denodeify(fs.readFile);
    return readFile(TOKEN_PATH).then(function(token) {
        oauth2Client.credentials = JSON.parse(token);
        return oauth2Client;
    });
}

function getAuthURL() {
    var oauth2Client = getAuthClient();
    return oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES
    });
}

function obtainAccessToken(code) {
    var authClient = getAuthClient();
    return new Promise(function(resolve, reject) {
        authClient.getToken(code, function(err, token) {
            if (err) {
                reject(err);
                return;
            }
            try {
                fs.mkdirSync(TOKEN_DIR);
            } catch (err) {
                if (err.code != 'EEXIST') {
                    throw err;
                }
            }
            fs.writeFile(TOKEN_PATH, JSON.stringify(token));
            console.log('Token stored to ' + TOKEN_PATH);
            resolve(token);
        });
    });
}