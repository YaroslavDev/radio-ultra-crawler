if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined' ? args[number] : match;
        });
    };
}

module.exports = {
    titleExistsInList: titleExistsInList
};

function titleExistsInList(title, songTitles) {
    title = title.toLowerCase().trim();
    return songTitles.some(function(songTitle) {
        return songTitle.toLowerCase().indexOf(title) > -1;
    });
}